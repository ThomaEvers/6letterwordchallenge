# 6 letter words

There's a file in the root of the repository, input.txt, that contains words of varying lengths (1 to 6 characters).

Your objective is to show all combinations of those words that together form a word of 6 characters. That combination must also be present in input.txt
E.g.:  
<code>
foobar  
fo  
obar  
</code>

should result in the ouput:  
<code>
fo+obar=foobar
</code>

You can start by only supporting combinations of two words and improve the algorithm at the end of the exercise to support any combinations.

Treat this exercise as if you were writing production code; think unit tests, SOLID, clean code and avoid primitive obsession. Be mindful of changing requirements like a different maximum combination length, or a different source of the input data.

The solution must be stored in a git repo. After the repo is cloned, the application should be able to run with one command / script.

Don't spend too much time on this.

# Startup

    dotnet run --project .\WordGenerator\WordGenerator.csproj

# running tests

    dotnet test

# Strategies

There is a simple and advanced strategy to combine words.

Simple strategy matches 2 words, resulting in 1 word
EXAMPLE:
<code>
fo+obar=foobar
</code>

Advanced strategy looks for combinations that can use a variable amount of words.
Since more values will exponentially grow the results of this strategy, it is best to not use it on the input.txt dataset, because it is incredibly slow. You can run the tests to see it in action.

"Why did you make this then?" => I just felt like it :)

EXAMPLES:
<code>
    fo+ob+ar=foobar
    f+o+o+b+ar=foobar
</code>