using System.Collections.Generic;
using System.Linq;

namespace _6LetterWordChallenge
{
    public class AdvancedStrategy : IStrategy
    {
        List<List<int>> combinations = new List<List<int>>();
        public HashSet<string> generateWords(Dictionary<int, List<string>> words, int maxWordLength)
        {
            combinations.Clear();
            HashSet<string> resultingWords = new HashSet<string>();
            if (words == null || maxWordLength == 0 || !words.ContainsKey(maxWordLength)) return resultingWords;
            FindCombinations(maxWordLength);
            List<List<List<string>>> wordCombinations = new List<List<List<string>>>();

            foreach (List<int> combination in combinations)
            {
                if (combination.Count == 1) continue;
                List<List<string>> wordCombination = new List<List<string>>();
                bool valid = true;
                foreach (int index in combination)
                {
                    if (words.ContainsKey(index)) wordCombination.Add(words[index].ToList());
                    else valid = false;
                }
                if (valid) wordCombinations.Add(wordCombination);
            }

            foreach (List<List<string>> wordCombination in wordCombinations)
            {
                IEnumerable<IEnumerable<string>> cartesianCombinations = CartesianProduct(wordCombination);
                foreach (IEnumerable<string> strings in cartesianCombinations)
                {
                    string res = "";
                    string resString = "";
                    foreach (string s in strings)
                    {
                        res += s;
                        resString = resString + "+" + s;
                    }
                    if (words[maxWordLength].Contains(res))
                    {
                        resultingWords.Add(resString.Substring(1) + "=" + res);
                    }
                }
            }

            return resultingWords;
        }
        void FindCombination(int[] arr, int index, int num, int reducedNum)
        {
            if (reducedNum < 0) return;

            if (reducedNum == 0)
            {
                List<int> numbersToGetTotal = new List<int>();
                for (int i = 0; i < index; i++) numbersToGetTotal.Add(arr[i]);
                combinations.Add(numbersToGetTotal);
                return;
            }

            int prev = (index == 0) ? 1 : arr[index - 1];

            for (int k = prev; k <= num; k++)
            {
                arr[index] = k;
                FindCombination(arr, index + 1, num, reducedNum - k);
            }
        }

        void FindCombinations(int n)
        {
            int[] arr = new int[n];
            FindCombination(arr, 0, n, n);
        }

        public static IEnumerable<IEnumerable<string>> CartesianProduct(IEnumerable<IEnumerable<string>> sequences)
        {
            if (sequences == null) return null;
            IEnumerable<IEnumerable<string>> emptyProduct = new[] { Enumerable.Empty<string>() };
            return sequences.Aggregate(emptyProduct, (accumulator, sequence) =>
                accumulator.SelectMany(accseq => sequence, (accseq, item) => accseq.Concat(new[] { item })));
        }
    }
}