using System.Collections.Generic;

namespace _6LetterWordChallenge
{
    public interface IStrategy
    {
        HashSet<string> generateWords(Dictionary<int, List<string>> words, int maxWordLength);
    }
}