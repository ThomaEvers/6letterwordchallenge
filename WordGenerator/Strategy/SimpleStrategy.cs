using System.Collections.Generic;

namespace _6LetterWordChallenge
{
    public class SimpleStrategy : IStrategy
    {
        public HashSet<string> generateWords(Dictionary<int, List<string>> words, int maxWordLength)
        {
            HashSet<string> resultingWords = new HashSet<string>();
            if (words == null || maxWordLength == 0 || !words.ContainsKey(maxWordLength)) return resultingWords;

            for (int i = 1; i <= (maxWordLength + 1) / 2; i++)
            {
                List<string> shortWords = words.ContainsKey(i) ? words[i] : new List<string>();
                List<string> longWords = words.ContainsKey(maxWordLength - i) ? words[maxWordLength - i] : new List<string>();

                if (!words.ContainsKey(maxWordLength)) continue;
                foreach (string shortWord in shortWords)
                {
                    foreach (string longWord in longWords)
                    {
                        if (words[maxWordLength].Contains(shortWord + longWord) && !resultingWords.Contains(shortWord + longWord))
                            resultingWords.Add(shortWord + "+" + longWord + "=" + shortWord + longWord);
                        if (words[maxWordLength].Contains(longWord + shortWord) && !resultingWords.Contains(longWord + shortWord))
                            resultingWords.Add(longWord + "+" + shortWord + "=" + longWord + shortWord);
                    }
                }

            }
            return resultingWords;
        }
    }
}