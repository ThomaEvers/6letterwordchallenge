using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _6LetterWordChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, List<string>> words = new Dictionary<int, List<string>>();
            int maxWordLength = 6;
            string inputUrl = "input.txt";
            IStrategy _strategy;

            try
            {
                // Open the text file using a stream reader.
                using (var sr = new StreamReader(inputUrl))
                {
                    foreach (string s in sr.ReadToEnd().Split())
                    {
                        if (words.ContainsKey(s.Length)) words[s.Length].Add(s);
                        else words.Add(s.Length, new List<string>() { s });
                    }

                    _strategy = new SimpleStrategy();
                    List<string> resultingWords = _strategy.generateWords(words, maxWordLength).ToList();
                    resultingWords.ForEach(w => System.Console.WriteLine(w));
                    System.Console.WriteLine("Total words found : " + resultingWords.Count);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }
    }
}

