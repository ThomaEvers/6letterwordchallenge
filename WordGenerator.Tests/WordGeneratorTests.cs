﻿using System;
using Xunit;
using _6LetterWordChallenge;
using System.Collections.Generic;
using System.Linq;

namespace _6LetterWordChallenge.UnitTests
{
    public class WordGeneratorTests
    {
        [Fact]
        public void SimpleStrategyTest()
        {
            IStrategy strategy = new SimpleStrategy();

            Dictionary<int, List<string>> words = new Dictionary<int, List<string>>();
            words.Add(3, new List<string>() { "foo", "bar", "kar" });
            words.Add(6, new List<string>() { "foobar", "fookar" });
            List<string> resultingWords = strategy.generateWords(words, 6).ToList();

            Assert.True(resultingWords.Count == 2, "We should have gotten 2 words back");
            Assert.Equal("foo+bar=foobar", resultingWords[0]);
            Assert.Equal("foo+kar=fookar", resultingWords[1]);
        }

        [Fact]
        public void SimpleStrategyEmptyWords()
        {
            IStrategy strategy = new SimpleStrategy();

            Dictionary<int, List<string>> words = new Dictionary<int, List<string>>();
            List<string> resultingWords = strategy.generateWords(words, 6).ToList();

            Assert.True(resultingWords.Count == 0, "We should have gotten 0 words back");
        }

        [Fact]
        public void SimpleStrategyNullWords()
        {
            IStrategy strategy = new SimpleStrategy();

            List<string> resultingWords = strategy.generateWords(null, 6).ToList();

            Assert.True(resultingWords.Count == 0, "We should have gotten 0 words back");
        }

        [Fact]
        public void SimpleStrategyNoWordsForLength()
        {
            IStrategy strategy = new SimpleStrategy();

            Dictionary<int, List<string>> words = new Dictionary<int, List<string>>();
            words.Add(3, new List<string>() { "foo", "bar", "kar" });
            words.Add(6, new List<string>() { "foobar", "fookar" });
            List<string> resultingWords = strategy.generateWords(words, 7).ToList();

            Assert.True(resultingWords.Count == 0, "We should have gotten 0 words back");
        }

        [Fact]
        public void AdvancedStrategyTest()
        {
            IStrategy strategy = new AdvancedStrategy();

            Dictionary<int, List<string>> words = new Dictionary<int, List<string>>();
            words.Add(1, new List<string>() { "f", "o", "o", "b", "a", "r" });
            words.Add(2, new List<string>() { "fo", "ar", "ob" });
            words.Add(3, new List<string>() { "foo", "bar" });
            words.Add(4, new List<string>() { "foob", "obar" });
            words.Add(6, new List<string>() { "foobar" });
            List<string> resultingWords = strategy.generateWords(words, 6).ToList();

            // For demo purpose
            resultingWords.ForEach(w => System.Console.WriteLine(w));

            Assert.True(resultingWords.Count == 8, "We should have gotten 8 words back");
        }

        [Fact]
        public void AdvancedStrategyEmptyWords()
        {
            IStrategy strategy = new AdvancedStrategy();

            Dictionary<int, List<string>> words = new Dictionary<int, List<string>>();
            List<string> resultingWords = strategy.generateWords(words, 6).ToList();

            Assert.True(resultingWords.Count == 0, "We should have gotten 0 words back");
        }

        [Fact]
        public void AdvancedStrategyNullList()
        {
            IStrategy strategy = new AdvancedStrategy();

            List<string> resultingWords = strategy.generateWords(null, 6).ToList();

            Assert.True(resultingWords.Count == 0, "We should have gotten 0 words back");
        }

        [Fact]
        public void AdvancedStrategyNoWordsForLength()
        {
            IStrategy strategy = new AdvancedStrategy();

            Dictionary<int, List<string>> words = new Dictionary<int, List<string>>();
            words.Add(3, new List<string>() { "foo", "bar", "kar" });
            words.Add(6, new List<string>() { "foobar", "fookar" });
            List<string> resultingWords = strategy.generateWords(words, 7).ToList();

            Assert.True(resultingWords.Count == 0, "We should have gotten 0 words back");
        }
    }
}
